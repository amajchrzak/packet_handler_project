# packet_handler_project

### short description: 
Project which simulates how packets work in network.

Project Realization:
Adam Majchrzak
Semester 5
Date 10.2020 - 12.2020
Program operations
<ul>
<li>Loading a file into the program, for testing I used a photo.</li>
<li>I split the image into 1000 byte packets, the last packet is the remainder from the split</i>
<li>For each packet I add a header to the beginning of the packet which is a structure, it consists of a unique packet ID and the packet size</li>
<li>On such prepared data I choose which checksum I want to calculate I have prepared a selection menu, which looks as follows Checksum selection menu 1 - parity bit -> 2 - modulo sum -> 3 - crc -> by assumption crc could be entered by the user, this version has been commented and I have imposed in advance that crc is 10010 when a different number than 1-3 will be counted for the parity bit After calculating the checksum you will see a menu with the disturbances for the packet</li>
<li>After calculating the checksum, a disturbance menu is displayed for the packet How many bits to disturb ? -user enters- Menu selects the type of file disturbance 1 - with repetitions 2 - without repetitions when entering a different number than 1-2 will count disturbances with repetitions</li>
<li>Then the controllerOfPackage function returns a vector in Boolean representation with all modifications and stores the vector containing each package separately</li>
<li>At the end data are converted to char type and new file is created</li>
</ul>

![Simple ethernet header](/cmake-build-debug/resources/ethernetFrame.png)
