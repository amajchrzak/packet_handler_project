#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <bitset>
#include <time.h>

using namespace std;

struct header
{
    int packetId;
    int dimensionOfPacket;
};

static int amountOfOneInBitRepresentation;
static int amountOfZeroInBitRepresentation;


void readFile(std::string nameOfFile, vector<char>& charRepresentation);
void writeToFile(vector<char> charRepresentation);

void menuChooseTypeOfChecksum(vector<bool>& packet, int choice);
bool parityBit(std::vector<bool> packet);
int sumModulo(std::vector<char> charRepresentationOfPacket, int moduloNumber);
vector<bool> crcFunction(std::vector<bool>& boolRepresentationOfPacket, vector<bool> crc);
void scoreOfFunctionCRC(std::vector<bool> boolRepresentation, vector<bool>& referenceBoolRepresentation, int lengthOfCRC);
void helperFunctionToSendBoolRepresentation(std::vector<bool> boolRepresentationOfPacket);

void menuChooseTypeOfDisorder(vector<bool>& packet, int choice, int amountBitsToDisorder);
void disorderBuffor(std::vector<bool>& headerAndDataBool, unsigned int amountOfDraws);
void disorderBufforWithoutRepeat(std::vector<bool>& headerAndDataBool, unsigned int amountOfDraws);
bool whetherSheWasDrawn(int placeToChange, std::vector<int> drawn, int amountOfDrawn);

vector<bool> fOnePacket(int id, int size);
vector<bool> controllerOfPackage(vector<bool>& bitRepresentation, vector<vector<bool>>* holdOnePacket);

void fBoolRepresentation(std::vector<bool>* boolRepresentation, std::bitset<8> bit1);
void fBoolRepresentationOfStruct(std::vector<bool>* boolRepresentation, std::bitset<32> bit1);
vector<char> fBoolToChar(vector<bool> boolRepresentation, vector<char>& charRepresentation);
//struct
void headerPacketBoolRepresentation(vector<bool>& boolRepresentation, int id, int size);


void readFile(std::string nameOfFile, vector<char>& charRepresentation)
{
    vector<bool> boolRepresentation;
    vector<vector<bool>> holdEachPacket;
    vector<bool> bufforOfPackets;
    streampos size;
    char* blockOfData;
    ifstream file(nameOfFile, ios::in | ios::binary | ios::ate);
    if (file.is_open()) {
        size = file.tellg();
        blockOfData = new char[size];
        file.seekg(0, ios::beg);
        file.read(blockOfData, size);
        for (int i = 0; i < size; i++) {
            fBoolRepresentation(&boolRepresentation, bitset<8>(blockOfData[i]));
        }
        bufforOfPackets = controllerOfPackage(boolRepresentation, &holdEachPacket);
        file.close();
        delete[] blockOfData;
    }
    else {
        cout << "Unable to open file.";
    }
    charRepresentation = fBoolToChar(bufforOfPackets, charRepresentation);
}
void writeToFile(vector<char> charRepresentation)
{
    fstream file;
    file.open("cat.bin", ios::out | ios::binary);

    if (file) {
        for (int i = 0; i < charRepresentation.size(); i++) {
            file << charRepresentation[i];
        }

        file.close();
    } else {
        cout << "Unable to open file.";
    }
}
void menuChooseTypeOfChecksum(vector<bool>& packet, int choice) {
    vector<bool> holdCRC;
    vector<bool> copyOfPacket;
    vector<bool> scoreOfCRC;
    switch (choice) {
        case 1:
            packet.push_back(parityBit(packet));
            break;
        case 2: {
            vector<char> computeModulo;
            computeModulo = fBoolToChar(packet, computeModulo);
            int scoreOfModulo = sumModulo(computeModulo, 26);
            fBoolRepresentation(&packet, bitset<8>(scoreOfModulo));
            break;
        }
        case 3:
            for (int i = 0; i < packet.size(); i++) {
                copyOfPacket.push_back(packet[i]);
            }
            for (int i = 0; i < 5; i++) {
                holdCRC = {true, false, false, true, false };
                if (i < (holdCRC.size() - 1)) {
                    copyOfPacket.push_back(false);
                }
            }
            copyOfPacket = crcFunction(copyOfPacket, holdCRC);
            scoreOfFunctionCRC(copyOfPacket, scoreOfCRC, holdCRC.size() - 1);

            for (int i = 0; i < scoreOfCRC.size(); i++) {
                packet.push_back(scoreOfCRC[i]);
            }
            break;
        default:
            packet.push_back(parityBit(packet));
            break;
    }
}
bool parityBit(std::vector<bool> packet) {
    for (int i = 0; i < packet.size(); i++) {
        if (packet[i]) {
            amountOfOneInBitRepresentation++;
        } else {
            amountOfZeroInBitRepresentation++;
        }
    }
    if (amountOfOneInBitRepresentation % 2 == 0) {
        return false;
    } else {
        return true;
    }
}
int sumModulo(std::vector<char> charRepresentationOfPacket, int moduloNumber) {
    unsigned int sumOfVector = 0;
    for (int i = 0; i < charRepresentationOfPacket.size(); i++) {
        sumOfVector += charRepresentationOfPacket[i];
    }
    int sumModulo = sumOfVector % moduloNumber;
    return sumModulo;
}
vector<bool> crcFunction(std::vector<bool>& boolRepresentationOfPacket, std::vector<bool> crc) {
    for (int i = 0; i < boolRepresentationOfPacket.size() - (crc.size() - 1); i++) {
        //helperFunctionToSendBoolRepresentation(boolRepresentationOfPacket);
        if (boolRepresentationOfPacket[i]) {
            for (int j = 0; j < crc.size(); j++) {
                if (crc[j] == boolRepresentationOfPacket[i + j]) {
                    boolRepresentationOfPacket[i + j] = false;
                }
                else {
                    boolRepresentationOfPacket[i + j] = true;
                }
            }
        }
    }
    return boolRepresentationOfPacket;
}
void helperFunctionToSendBoolRepresentation(std::vector<bool> boolRepresentationOfPacket) {
    for (int i = 0; i < boolRepresentationOfPacket.size(); i++) {
        if (boolRepresentationOfPacket[i]) {
            std::cout << "1";
        }
        else {
            std::cout << "0";
        }
    }
}
void scoreOfFunctionCRC(std::vector<bool> boolRepresentation, std::vector<bool>& referenceBoolRepresentation, int lengthOfCRC) {
    for (int i = 0; i < lengthOfCRC; i++) {
        if (boolRepresentation[boolRepresentation.size() - i - 1]) {
            referenceBoolRepresentation.push_back(true);
        }
        else
        {
            referenceBoolRepresentation.push_back(false);
        }
    }
}

void menuChooseTypeOfDisorder(vector<bool>& packet, int choice, int amountBitsToDisorder) {
    switch (choice)
    {
        case 1:
            disorderBuffor(packet, amountBitsToDisorder);
            break;
        case 2:
            disorderBufforWithoutRepeat(packet, amountBitsToDisorder);
            break;
        default:
            disorderBuffor(packet, amountBitsToDisorder);
            break;
    }
}
void disorderBuffor(std::vector<bool>& headerAndDataBool, unsigned int amountOfDraws)
{
    srand(time(NULL));
    for (int i = 0; i < amountOfDraws; i++) {
        int placeToChange = rand() % headerAndDataBool.size();

        if (headerAndDataBool[placeToChange]) {
            headerAndDataBool[placeToChange] = false;
        } else {
            headerAndDataBool[placeToChange] = true;
        }
    }
}
void disorderBufforWithoutRepeat(std::vector<bool>& headerAndDataBool, unsigned int amountOfDraws)
{
    srand(time(NULL));
    int amountOfDrawn = 0;
    std::vector<int> drawn;
    do {
        int placeToChange = rand() % headerAndDataBool.size();
        if (!whetherSheWasDrawn(placeToChange, drawn, amountOfDrawn)) {
            drawn.push_back(placeToChange);
            amountOfDrawn++;
            if (headerAndDataBool[placeToChange]) {
                headerAndDataBool[placeToChange] = false;
            } else {
                headerAndDataBool[placeToChange] = true;
            }
        }
    } while (amountOfDrawn < amountOfDraws);
    amountOfDrawn = 0;
    do {
        amountOfDrawn++;
    } while (amountOfDrawn < amountOfDraws);
}
bool whetherSheWasDrawn(int placeToChange, std::vector<int> drawn, int amountOfDrawn)
{
    if (amountOfDrawn <= 0) {
        return false;
    }
    int i = 0;
    do {
        if (drawn[i] == placeToChange) {
            return true;
        }
        i++;
    } while (i < amountOfDrawn);

    return false;
}

vector<bool> controllerOfPackage(vector<bool>& boolRepresentation, vector<vector<bool>>* holdOnePacket) {
    int choiceOfChecksum;
    int disorderChoice;
    int bitsToDisorder;
    {
        cout << "Choose checksum" << endl << "1 - parity bite" << endl << "2 - sum modulo" << endl << "3 - crc" << endl;
    }
    cin >> choiceOfChecksum;
    {
        cout << "How many bits disorder ?";
    }
    cin >> bitsToDisorder;
    {
        cout << "Choose type of disorder" << endl << "1 - with repeat" << endl << "2 - without repeat" << endl;
    }
    cin >> disorderChoice;

    int i = 0;
    vector<bool> bufforPackets;
    int sizeOfOnePacket = 1000;
    int restOfBytes = boolRepresentation.size() % 40;
    int amountOfPackets = (boolRepresentation.size() / sizeOfOnePacket) + 1;
    for (int j = 1; j <= amountOfPackets; j++) {
        vector<bool> packets = fOnePacket(j, sizeOfOnePacket);
        //suma modulo inty
        while (i < boolRepresentation.size())
        {
            packets.push_back(boolRepresentation[i]);
            if ((i + 1) % sizeOfOnePacket == 0) {
                menuChooseTypeOfChecksum(packets, choiceOfChecksum);
                menuChooseTypeOfDisorder(packets, choiceOfChecksum, bitsToDisorder);
                for (int k = 0; k < packets.size(); k++) {
                    bufforPackets.push_back(packets[k]);
                }
                i++;
                break;
            } else {
                i++;
            }
            if ((i == (boolRepresentation.size() - 1)) && ((boolRepresentation.size() - ((j - 1) * sizeOfOnePacket)) % restOfBytes == 0)) {
                menuChooseTypeOfChecksum(packets, choiceOfChecksum);
                menuChooseTypeOfDisorder(packets, choiceOfChecksum, bitsToDisorder);
                for (int k = 0; k < packets.size(); k++) {
                    bufforPackets.push_back(packets[k]);
                }
                break;
            }
        }
        holdOnePacket->push_back(packets);
    }

    return bufforPackets;
}
vector<bool> fOnePacket(int id, int size)
{
    vector<bool> packet;
    headerPacketBoolRepresentation(packet, id, size);

    return packet;
}

void fBoolRepresentation(std::vector<bool>* boolRepresentation, std::bitset<8> bit1) {
    for (int i = 7; i >= 0; i--)
    {
        if (bit1[i] == 1) {
            boolRepresentation->push_back(true);
        } else {
            boolRepresentation->push_back(false);
        }
    }
}
void fBoolRepresentationOfStruct(std::vector<bool>* boolRepresentation, std::bitset<32> bit1) {
    for (int i = 7; i >= 0; i--)
    {
        if (bit1[i] == 1) {
            boolRepresentation->push_back(true);
        } else {
            boolRepresentation->push_back(false);
        }
    }
    for (int i = 31; i >= 8; i--)
    {
        if (bit1[i] == 1) {
            boolRepresentation->push_back(true);
        } else {
            boolRepresentation->push_back(false);
        }
    }
}
vector<char> fBoolToChar(vector<bool> boolRepresentation, vector<char>& charRepresentation)
{
    vector<bitset < 8 >> bitsetRepr;
    int amountOfIterations = boolRepresentation.size() / 8;
    char** strOut = new char* [amountOfIterations];
    for (int i = 0; i < amountOfIterations; i++) {
        strOut[i] = new char[8];
    }

    int k = 0;
    for (int j = 0; j < amountOfIterations; j++) {
        for (int i = 0; i < 8; i++) {
            strOut[j][i] = boolRepresentation[k] ? '1' : '0';
            k++;
        }
        strOut[j][8] = '\0';
        bitsetRepr.push_back(bitset < 8 >(strOut[j]));
    }

    for (auto iter : bitsetRepr)
    {
        charRepresentation.push_back(static_cast<char>((iter).to_ulong()));
    }
    delete[] strOut;

    return charRepresentation;
}

void headerPacketBoolRepresentation(vector<bool>& boolRepresentation, int id, int size)
{
    header headerOfPacket = {id, size };
    fBoolRepresentationOfStruct(&boolRepresentation, bitset<32>(headerOfPacket.packetId));
    fBoolRepresentationOfStruct(&boolRepresentation, bitset<32>(headerOfPacket.dimensionOfPacket));
}

int main()
{
    vector<char> charRepresentation;
    readFile("cat.bin", charRepresentation);
    writeToFile(charRepresentation);
    return 0;
}
